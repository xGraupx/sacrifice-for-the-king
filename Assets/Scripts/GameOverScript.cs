﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScript : MonoBehaviour {

    [SerializeField] GameObject MenuScene, CanvasAnimations;

    private float delay;

    private void OnEnable()
    {
        delay = 0.2f;
    }

    private void Update()
    {
        if (delay <= 0)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount == 1)
            {
                MenuScene.SetActive(true);
                CanvasAnimations.GetComponent<Animator>().enabled = true;
                CanvasAnimations.GetComponent<Animator>().Play("GameOverClick");
                this.gameObject.SetActive(false);
            }
        }
        else
            delay -= Time.deltaTime;
    }
}
