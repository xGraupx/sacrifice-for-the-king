﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour {

    [SerializeField] GameObject GameOver;
    [SerializeField] GameObject Pause;
    [SerializeField] GameObject PauseScore;
    [SerializeField] GameObject PauseBestScore;
    [SerializeField] GameObject AnimationsCanvas;
    [SerializeField] GameObject TotalScore;
    [SerializeField] GameObject needItemsText;
    [SerializeField] GameObject ProgresBar;
    [SerializeField] GameObject HandImage;
    [SerializeField] GameObject Coins, Dishes, Fruits, Flowers;
    [SerializeField] GameObject bestScore, bestScoreAnimation, scoreEnd, scoreAnimation;
    [SerializeField] float lvlTime = 20;
    [SerializeField] int FirstlvlItems = 3;
    [SerializeField] float StartAnimationDelay = 5;

    public bool[] need = new bool[4];
    public int item;
    private float lvl;
    private int needItems;
    private int score;
    private int BestScore;
    private int actualItems;
    private float Delay;
    private float AnimationDelay;
    private float delaySkipAnimation;
    private int progresBar;

    private bool start;
    private Vector2 StartPos;
    [SerializeField] GameObject Items;

    private void OnEnable()
    {
        AnimationsCanvas.GetComponent<Animator>().Play("None");
    }

    private void Start()
    {
        delaySkipAnimation = 0.2f;
        StartPos = Items.transform.position;
        if (PlayerPrefs.HasKey("dwamkgrkmklvsc"))
        {
            BestScore = PlayerPrefs.GetInt("dwamkgrkmklvsc");
        }
        else
            BestScore = 0;
        bestScore.GetComponent<Text>().text = Convert.ToString("BEST: " + BestScore);
        bestScoreAnimation.GetComponent<Text>().text = Convert.ToString("BEST: " + BestScore);
    }

    public void SetStart()
    {
        delaySkipAnimation = 0.2f;
        lvl = 1;
        score = 0;
        actualItems = 0;
        needItems = FirstlvlItems;
        AnimationDelay = StartAnimationDelay;
        this.gameObject.GetComponent<Animator>().enabled = true;
        start = false;
        nextLvl();
    }

    private void FixedUpdate()
    {
        if (start)
            Items.transform.position = Input.mousePosition;

        if (delaySkipAnimation <= 0)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount == 1)
            {
                if (this.gameObject.GetComponent<Animator>().enabled == true)
                {
                    this.gameObject.GetComponent<Animator>().enabled = false;
                    HandImage.SetActive(false);
                }
            }
        }
        else
            delaySkipAnimation -= Time.deltaTime;

        if (Delay > 0)
        {
            Delay -= Time.deltaTime;
            ProgresBar.GetComponent<RectTransform>().offsetMin = new Vector2((800 - 800 * (Delay / lvlTime))-350, -655);
            needItemsText.GetComponent<Text>().text = actualItems + "/" + needItems;
            TotalScore.GetComponent<Text>().text = Convert.ToString(score);
        }
        else if(actualItems >= needItems)
        {
            AnimationsCanvas.GetComponent<Animator>().enabled = true;
            AnimationsCanvas.GetComponent<Animator>().Play("ResultsGood");
            if (AnimationDelay <= 0)
            {
                AnimationsCanvas.GetComponent<Animator>().Play("None");
                lvl++;
                actualItems = 0;
                nextLvl();
                ProgresBar.GetComponent<RectTransform>().offsetMin = new Vector2(-350, -350);
                AnimationDelay = StartAnimationDelay;
            }
            else
                AnimationDelay -= Time.deltaTime;
        }
        else
        {
            AnimationsCanvas.GetComponent<Animator>().enabled = true;
            AnimationsCanvas.GetComponent<Animator>().Play("ResultsBad");
            if (AnimationDelay <= 0)
            {
                AnimationDelay = StartAnimationDelay;
                GameOver.SetActive(true);
                if (score > BestScore)
                {
                    BestScore = score;
                    PlayerPrefs.SetInt("dwamkgrkmklvsc", score);
                    PlayerPrefs.Save();

                }
                scoreEnd.GetComponent<Text>().text = Convert.ToString(score);
                scoreAnimation.GetComponent<Text>().text = Convert.ToString(score);
                this.gameObject.SetActive(false);
            }
            else
                AnimationDelay -= Time.deltaTime;
        }
    }

    private void nextLvl()
    {
        Delay = lvlTime;
        do
        {
            for (int i = 0; i < 4; i++)
            {
                need[i] = Convert.ToBoolean(UnityEngine.Random.Range(0, 2));
            }
        } while (need[0] == need[1] == need[2] == need[3]);
        ImageNeed(Coins, need[0]);
        ImageNeed(Dishes, need[1]);
        ImageNeed(Fruits, need[2]);
        ImageNeed(Flowers, need[3]);
        int x = 0;
        for (int i = 0; i < 4; i++)
        {
            if (need[i])
                x++;
        }
        needItems = FirstlvlItems + Convert.ToInt32(lvl * x);
        GenerateItem();
        lvl += 0.1f;
    }

    private void ImageNeed(GameObject item, bool ifNeedTrue)
    {
        item.transform.Find("v").gameObject.SetActive(ifNeedTrue);
        item.transform.Find("x").gameObject.SetActive(!ifNeedTrue);
    }

    private void GenerateItem()
    {
        int number = Convert.ToInt32(UnityEngine.Random.Range(-1, 8));
        if (number == -1)
            number++;
        item = number;
        for (int i = 0; i < Items.transform.childCount; i++)
        {
            Items.transform.GetChild(i).gameObject.SetActive(false);
        }
        Items.transform.GetChild(number).gameObject.SetActive(true);
    }

    public void MouseDown()
    {
        start = true;
    }

    public void MouseUp()
    {
        float PosX = StartPos.x - Items.transform.position.x;
        float PosY = StartPos.y - Items.transform.position.y;
        
        int x = Convert.ToInt32(Screen.height * 0.1f);
        int y = Convert.ToInt32(Screen.height * (9.0f / 16) * (1.0f / 5f));
        if (PosY < x && PosY > -x)
        {
            if (PosX > y)
            {
                GenerateItem();
            }
            else if (PosX < -y)
            {
                switch(item)
                {
                    case 0:
                        if (need[0] == true)
                        {
                            actualItems++;
                            score++;
                        }
                        else
                        {
                            actualItems -= 2;
                            score -= 2;
                        }
                        break;
                    case 1:
                    case 2:
                        if (need[1] == true)
                        {
                            actualItems++;
                            score++;
                        }
                        else
                        {
                            actualItems -= 2;
                            score -= 2;
                        }
                        break;
                    case 3:
                    case 4:
                    case 5:
                        if (need[2] == true)
                        {
                            actualItems++;
                            score++;
                        }
                        else
                        {
                            actualItems -= 2;
                            score -= 2;
                        }
                        break;
                    case 6:
                    case 7:
                        if (need[3] == true)
                        {
                            actualItems++;
                            score++;
                        }
                        else
                        {
                            actualItems -= 2;
                            score -= 2;
                        }
                        break;
                }
                GenerateItem();
            }
        }
        start = false;
        Items.transform.position = StartPos;
    }

    public void PauseClick()
    {
        PauseBestScore.GetComponent<Text>().text = Convert.ToString("BEST: " + BestScore);
        PauseScore.GetComponent<Text>().text = Convert.ToString("SCORE: " + score);
        Pause.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
