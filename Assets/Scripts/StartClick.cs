﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StartClick : MonoBehaviour
{

    [SerializeField] GameObject GameScene, CanvasAnimations;
    [SerializeField] GameObject Score;
    [SerializeField] GameObject BestScoreAnimation;

    private float delay;

    private void OnEnable()
    {
        CanvasAnimations.GetComponent<Animator>().enabled = true;
        CanvasAnimations.GetComponent<Animator>().Play("None");
        delay = 0.2f;
        int BestScore = 0;
        if (PlayerPrefs.HasKey("dwamkgrkmklvsc"))
        {
            BestScore = PlayerPrefs.GetInt("dwamkgrkmklvsc");
        }
        Score.GetComponent<Text>().text = Convert.ToString("BEST: " + BestScore);
        BestScoreAnimation.GetComponent<Text>().text = Convert.ToString("BEST: " + BestScore);
    }

    private void Update()
    {
        if (delay <= 0)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount == 1)
            {
                if (Input.mousePosition.y < (Screen.height / 1.1f))
                {
                    GameScene.SetActive(true);
                    CanvasAnimations.GetComponent<Animator>().enabled = true;
                    GameScene.GetComponent<Game>().SetStart();
                    CanvasAnimations.GetComponent<Animator>().Play("MenuClick");
                    this.gameObject.SetActive(false);
                }
            }
        }
        else
            delay -= Time.deltaTime;
    }

    public void exit()
    {
        Application.Quit();
    }
}
