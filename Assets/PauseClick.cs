﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseClick : MonoBehaviour {

    [SerializeField] GameObject GameScene, MenuScene;

    private float delay;

    private void OnEnable()
    {
        delay = 0.2f;
    }

    private void Update()
    {
        if (delay <= 0)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount == 1)
            {
                if (Input.mousePosition.y < (Screen.height / 1.1f))
                {
                    GameScene.SetActive(true);
                    this.gameObject.SetActive(false);
                }
            }
        }
        else
            delay -= Time.deltaTime;
    }

    public void MenuClick()
    {
        GameScene.SetActive(false);
        MenuScene.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
